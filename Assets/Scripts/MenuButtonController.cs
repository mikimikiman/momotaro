﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtonController : MonoBehaviour
{
    public AudioClip menuSound;
    public AudioClip gameSound;
    public AudioClip explanationSound;
    public AudioClip titleSound;
    AudioSource audioSource;

    // メニューボタンからシーン移動
    public void ChangeScene(string scene)
    {
        switch (scene)
        {
            case "Title" :
                // タイトル画面に移動
                audioSource.PlayOneShot(titleSound);
                Invoke("TitleScene", 2.5f);
                break;
            case "GameSelect" :
                // ゲームセレクト画面に移動
                audioSource.PlayOneShot(menuSound);
                SceneManager.LoadScene("GameSelect");
                break;
            case "RiverDescent" :
                // どんぶらこに移動
                audioSource.PlayOneShot(gameSound);
                Invoke("RiverDescentScene", 1.5f);
                break;
            case "OniIsland":
                // 鬼退治に移動
                audioSource.PlayOneShot(explanationSound);
                SceneManager.LoadScene("OniIsland");
                break;
            case "Explanation":
                // 鬼退治説明
                audioSource.PlayOneShot(gameSound);
                Invoke("ExplanationScene", 1.5f);
                break;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void TitleScene()
    {
        SceneManager.LoadScene("Title");
    }

    void RiverDescentScene()
    {
        SceneManager.LoadScene("RiverDescent");
    }

    void ExplanationScene()
    {
        SceneManager.LoadScene("Explanation");
    }
}
