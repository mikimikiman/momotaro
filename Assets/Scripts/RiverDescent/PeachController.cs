﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeachController : MonoBehaviour
{
    // 桃のオブジェクトをアタッチ
    public GameObject peach;

    // 桃が動く方向
    Vector3 moveDirection = Vector3.zero;

    // 前後のMAXスピード
    public float speedZ;

    // 桃が前後へ進む加速度
    public float accelerationZ;

    // 左右のMAXスピード
    public float speedX;

    // 桃が左右へ進む加速度
    public float accelerationX;

    // 桃のMAXライフ
    const int maxLife = 3;

    // 桃のライフ
    int life = maxLife;

    // ダメージ用アニメーション
    Animator animator;

    // ダメージ用効果音
    AudioSource damageSound;

    private Rigidbody peachRigidbody;
    public float peachClampSpeed;
    public float moveSpeed = 5;

    // Start is called before the first frame update
    void Start()
    {
        // アニメーターのコンポーネント取得
        animator = GetComponent<Animator>();

        // Rigidbody呼び出し
        peachRigidbody = this.gameObject.GetComponent<Rigidbody>();

        // 音楽のコンポーネント取得
        damageSound = GetComponent<AudioSource>();
    }

    // 物理と相性がいいのがFixedUpdate
    void FixedUpdate()
    {

        moveDirection.x = Input.GetAxisRaw("Horizontal");
        moveDirection.z = Input.GetAxisRaw("Vertical");
        moveDirection.y = -0.5f;
        moveDirection.Normalize();

        peachRigidbody.velocity = moveDirection * moveSpeed;

        //// 上下左右移動
        //if (Input.GetKey(KeyCode.LeftArrow))
        //{
        //    float acceleratedX = moveDirection.x + (accelerationX * Time.deltaTime);
        //    moveDirection.x = Mathf.Clamp(acceleratedX, 0, speedX);
        //    peachRigidbody.AddForce(-moveDirection.x * speedX, 0, 0);
        //}

        //if (Input.GetKey(KeyCode.RightArrow))
        //{
        //    float acceleratedX = moveDirection.x + (accelerationX * Time.deltaTime);
        //    moveDirection.x = Mathf.Clamp(acceleratedX, 0, speedX);
        //    peachRigidbody.AddForce(moveDirection.x * speedX, 0, 0);
        //}
        //if (Input.GetKey(KeyCode.UpArrow))
        //{
        //    float acceleratedZ = moveDirection.z + (accelerationZ * Time.deltaTime);
        //    moveDirection.z = Mathf.Clamp(acceleratedZ, 0, speedZ);
        //    peachRigidbody.AddForce(0, 0, moveDirection.z * speedZ);
        //}

        //if (Input.GetKey(KeyCode.DownArrow))
        //{
        //    float acceleratedZ = moveDirection.z + (accelerationZ * Time.deltaTime);
        //    moveDirection.z = Mathf.Clamp(acceleratedZ, 0, speedZ);
        //    peachRigidbody.AddForce(0, 0, -moveDirection.z * speedZ);
        //}

        //if ( Mathf.Abs(peachRigidbody.velocity.z) > peachClampSpeed )
        //{
        //    peachRigidbody.velocity = new Vector3(peachRigidbody.velocity.x, peachRigidbody.velocity.y, peachClampSpeed);
        //}
        //if (Mathf.Abs(peachRigidbody.velocity.x) > 100)
        //{
        //    peachRigidbody.velocity = new Vector3(peachClampSpeed, peachRigidbody.velocity.y, peachRigidbody.velocity.z);
        //}
    }

    // ライフの数呼び出し
    public int Life()
    {
        return life;
    }

    // 敵キャラにぶつかったときの処理
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            // ライフを１減らす
            life--;

            // 敵消す
            Destroy(collision.gameObject);

            // ダメージ用音楽再生
            damageSound.Play();

            // ダメージを受けた時のアニメーション
            this.animator.SetTrigger("PeachDamage");
        }
    }
    
}
