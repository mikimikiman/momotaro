﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearAreaController : MonoBehaviour
{
    public GameObject BearPrefab;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Momo")
        {
            // 桃のz座標を取得
            GameObject peach = GameObject.Find("Peach");
            peach.GetComponent<Transform>();
            Vector3 peachZ = peach.transform.position;

            // x座標に熊がランダムで現れる
            GameObject bear = Instantiate(BearPrefab) as GameObject;
            int x = Random.Range(96, 107);
            bear.transform.position = new Vector3(x, 5, peachZ.z + 5);
        }
    }
}
