﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public PeachController peach;
    public LifePanel lifePanel;
    public GameObject gameOverText;
    AudioSource gameOver;

    // Start is called before the first frame update
    void Start()
    {
        gameOver = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // ライフパネルを更新
        lifePanel.UpdateLife(peach.Life());

        // 桃のライフが０になったらゲームオーバー
        if (peach.Life() <= 0)
        {
            // Updateの更新をオフにしてコンポーネントをオフにする
            enabled = false;

            // ゲームオーバーの音楽を流す
            gameOver.Play();

            // ゲームオーバーの文字を表示する
            gameOverText.SetActive(true);

            // 5秒後にReturnToTitleを呼び出す
            Invoke("ReturnToMenu", 5.0f);
        }
    }

    void ReturnToMenu()
    {
        // メニュー画面呼び出し
        SceneManager.LoadScene("GameSelect");
    }
}
