﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    // マップのオブジェクトをアタッチ
    public GameObject mapPlayer;

    // シーン初期位置
    Vector3 startPos;

    // Start is called before the first frame update
    void Start()
    {
        // シーン初期座標
        startPos = mapPlayer.transform.position;

    }

    // Update is called once per frame
    void Update()
    {

    }
}
