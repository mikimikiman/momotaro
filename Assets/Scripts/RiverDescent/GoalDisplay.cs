﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalDisplay : MonoBehaviour
{
    public GameObject GoalText;
    AudioSource clearSound;

    // Start is called before the first frame update
    void Start()
    {
        GoalText.SetActive(false);
        clearSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Momo")
        {
            // Goalの文字を表示
            GoalText.SetActive(true);

            // 音楽をながす
            clearSound.Play();

            Invoke("SecondStage", 3.0f);
        }
    }

    void SecondStage()
    {
        SceneManager.LoadScene("Explanation");
    }
}
