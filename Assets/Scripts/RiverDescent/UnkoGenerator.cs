﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UnkoGenerator : MonoBehaviour
{
    public GameObject UnkoPrefab;
    float interval = 0.5f; // 0.5秒の間隔
    float time = 0; // 時間のカウント

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerStay (Collider other)
    {
        // フレーム間の経過した時間を変数timeに代入
        this.time += Time.deltaTime;

        // もし変数timeの値が変数intervalを超えたら
        if (this.time > this.interval)
        {
            // 経過時間を初期化
            this.time = 0;

            // 桃のz座標を取得
            GameObject peach = GameObject.Find("Peach");
            peach.GetComponent<Transform>();
            Vector3 peachZ = peach.transform.position;

            // x座標にうんこがランダムに降ってくる
            GameObject unko = Instantiate(UnkoPrefab) as GameObject;
            int x = Random.Range(145, 187);
            unko.transform.position = new Vector3(x, 10, peachZ.z + 5);
        }
    }
}
