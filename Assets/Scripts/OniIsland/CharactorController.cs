﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CharactorController : MonoBehaviour
{
    GameObject clickedGameObject;
    AudioSource audioSource;
    public AudioClip attack;
    public AudioClip damage;
    public GameObject attackImage;

    // MAXライフ
    const int maxLife = 3;

    // 現在のライフ
    int life = maxLife;

    // 鬼のMAXカウント
    const int maxOniCount = 10;

    // 鬼の現在のカウント
    int oniCount = maxOniCount;

    // 秒数設定
    float exchangeImage;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // クリックしたらキャラクターを消す
        if (Input.GetMouseButtonDown(0))
        {
            clickedGameObject = null;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit))
            {
                clickedGameObject = hit.collider.gameObject;

                // 鬼をクリックしたらカウントをマイナス
                if (clickedGameObject.tag == "Enemy")
                {
                    exchangeImage += Time.deltaTime;
                    oniCount--;
                    audioSource.PlayOneShot(attack);
                    attackImage.SetActive(true);
                    //if (exchangeImage >= 1.0f)
                    //{
                    //    exchangeImage = 0;
                    //    attackImage.SetActive(false);
                    //}

                }

                // 鬼以外はおてつき
                if (clickedGameObject.tag == "Citizen")
                {
                    life--;
                    audioSource.PlayOneShot(damage);
                }
                Destroy(clickedGameObject);
            }
        }

        // 下まで落ちたらキャラクター消す
        if (this.transform.position.y < -30f)
        {
            Destroy(this.gameObject);
        }
    }

    public int Life()
    {
        return life;
    }

    public int EnemyCount()
    {
        if (oniCount <= 0)
        {
            oniCount = 0;
        }
        return oniCount;
    }
}
