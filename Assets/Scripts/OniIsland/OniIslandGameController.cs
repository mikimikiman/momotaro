﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OniIslandGameController : MonoBehaviour
{
    public CharactorController chara;
    //public GameObject charactars;
    public Text counter;
    public GameObject gameClear;
    public GameObject gameOver;
    public LifeAndCountPanel lifeAndCountPanel;
    AudioSource audioSource;
    public AudioClip clearSound;
    public AudioClip gameOverSound;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // 鬼カウンターを更新
        counter.text = "あと　" + chara.EnemyCount() + "匹";

        // ライフパネルを更新
        lifeAndCountPanel.UpdateLife(chara.Life());

        // 鬼カウントが0になったらゲームクリア
        if (chara.EnemyCount() <= 0)
        {
            // ゲームクリアのサウンド
            audioSource.PlayOneShot(clearSound);

            // GAME CLEAR!の文字を表示する
            gameClear.SetActive(true);

            // 3秒後にエンディング呼び出し
            Invoke("Ending", 3.0f);
        }

        // ライフが0になったらゲームオーバー
        if (chara.Life() <= 0)
        {
            // これ以降のライフのアップデートはやめる
            enabled = false;

            // ゲームオーバーのサウンド
            audioSource.PlayOneShot(gameOverSound);
            // ゲームオーバーの文字を表示する
            gameOver.SetActive(true);

            // 3秒後にメニュータイトル呼び出し
            Invoke("ReturnToMenu", 5.0f);
        }
    }

    void Ending()
    {
        SceneManager.LoadScene("ThanksEnding");
    }

    void ReturnToMenu()
    {
        SceneManager.LoadScene("GameSelect");
    }
}
