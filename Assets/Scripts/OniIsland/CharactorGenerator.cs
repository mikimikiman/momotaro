﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactorGenerator : MonoBehaviour
{
    public GameObject[] charactars;

    private GameObject instantiateChara;

    public float fallSpeed;
    

    // Start is called before the first frame update
    void Start()
    {
        // InvokeRepeating("関数名,初回呼び出しまでの秒数,次回呼び出しまでの秒数)
        InvokeRepeating("CharactorSet", 1, 1);
        
    }

    // Update is called once per frame
    void Update()
    {
        // キャラクターを落とす
        if (instantiateChara != null)
        {
            instantiateChara.transform.Translate(0, -fallSpeed * Time.deltaTime, 0);
        }
    }

    void CharactorSet()
    {
        // 各キャラがランダムに降りてくる
        int randomCharactar = Random.Range(0, charactars.Length);
        int x = Random.Range(-20, 15);

        GameObject selectChara = charactars[randomCharactar];
        instantiateChara = Instantiate(selectChara, new Vector3(x, 25, -20), Quaternion.identity);
    }
}
